// Task 14
//----------------------------------------------------------------

// // 1. Accepts 2 arguments and multiplies the two arguments and returns the value
// //-------------------------------------------------------------
// let first = 3;
// let second = 5;
// function firstMultiplier(){
//     let multiply = first * second;
//     console.log(multiply);
// }


// // 2. Accept 3 Arguments and adds the 3 arguments and returns the value
// //-------------------------------------------------------------
// let first = 4;
// let second = 5;
// let third = 10;
// function secondAddition(){
//     let addition = first + second + third;
// }


// // 3. Accept 1 argument and divides the argument by 2 and returns the value
// //-------------------------------------------------------------
// let third = 10
// function thirdDivition(){
//     let divide = first / 2;
// }


// // 4. Accepts 3 arguments, multiplies the sum of the first two arguments with the third and returns the value
// //-------------------------------------------------------------
// let first = 4;
// let second = 5;
// let third = 10;
// function secondAddition(){
//     let miltiplyAndSum = (first + second) * third;
// }


// // 5. Illustrate using code how the block scoped variables work
// //   - Use comments to explain what is happening
// //-------------------------------------------------------------
// // Defining variable first
// let first = "1"; 
// // See if variable first is equal to 1
// if(first == 1){
//     // Defining variable second inside of blocked scope
//     let second = "2"; // aka blocked
// }
// // Printing vaiable second that have not been defines before as a variable
// console.log(second); 


// // 6. Illustrate using code how the Hoisting work
// //   - Use comments to explain what is happening
// //-------------------------------------------------------------
// //      1- All variables are set to undefined
// //      3- Variables are set as defined
// let second = "world";
// let first = "Hello ";
// //      4- Function is called
// printFirst();
// //      2- All functions are hoisted/moved to the top
// function printFirst(){
//     let printout = first + second;
// }


// // 7. In code give an example of Coercion using diffrent data types
// //  - Come up with at least 5 examples
// //-------------------------------------------------------------
// let name = "Darth ";
// let number = 1337;
// let darkside = true;
// let happiness = null;
// let jedi = happiness + 1337;
// function nameAndNumber(){
//     let combine = name + number;
// }
// function darksideHappiness(){
//     let combine = "You feel the darkside is "+ darkside + " and it makes you feel " + happiness;
// }


// // 8. Write an employee function that can be invoked using new keyword.
// const newEmplyee = new person("12345", "Kalles", "Kaviar", "kalles@kaviar.com");

// function person(id, name, surname, email) {
//     this.id = id,
//     this.name = name,
//     this.surname = surname,
//     this.email = email,
//     this.fullName = function() {
//         return this.name + " " + this.surname;
//     };
//     this.contactCard = {
//         empid: this.id,
//         uname: this.name,
//         LastName: this.surname
//     };
// };


// // 9. Create an Object that represents a project
// const project = {
// name: "Disaster Project",
// description: "This is a disaster",
// process: "R&D",
// startDate: "2020-02-25",
// budget: 10,
// active: true,
// projectMembers:{
//     projectManagerName: "Batman",
//     architect: "Wonder Woman",
//     developer: "Superman"
//     }
// }